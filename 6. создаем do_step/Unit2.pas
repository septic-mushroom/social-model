unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids;

type

  Tcell = class
  public
    man: record //������ ��������� �������� ( �������?)
      srtong:integer;  //
      charisma:integer;
    end;
    women: record
      beauty:integer;
    end;
    exist;
    Resource: integer;
    Constructor Create();
  end;

  TField = class // ��� ����� ���������� ��������� � ����������1
  private
    FWidth: integer; // ������ ���� � �������
    FHeight: integer; // ������ ���� � �������
    maxRes: integer; //�������� �������� ������ ���� ������

    Fstep: integer;
    //procedure GrowAllCells;
    function findMaxResource(startX, startY: integer): integer;
    function IsInside(X, Y: Integer): Boolean;

    procedure makeCellExist(X, Y: integer; Resource: integer);

  public

    // �����������.
    FArea: array of array of Tcell; // ������ ������ ����
    procedure GrowAllCells;
    constructor Create(Width, Height: integer);
    // ����������.

    procedure fillField(rowCount, colCount, maxValue: integer);
    procedure DoStep(Fstep: integer);
    // procedure fill(var sg: TStringGrid);
  end;

  TForm2 = class(TForm)
    StringGrid1: TStringGrid;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    // ; ACol, ARow: Integer;   Rect: TRect; State: TGridDrawState);
    procedure StringGrid1SelectCell(Sender: TObject; ACol, ARow: integer;
      var CanSelect: boolean);
    procedure StringGrid1DrawCell(Field:Tfield; Sender: TObject; ACol, ARow: integer;
      Rect: TRect; State: TGridDrawState);

  private
    { Private declarations }


    procedure fillStiringGrid(var field: TField; var sg: TStringGrid;
      rowCount, colCount, maxValue: integer);
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}
function TField.IsInside(X, Y: Integer): Boolean;
begin
  Result := (X >= 0) and (X < FWidth) and (Y >= 0) and (Y < FHeight);
end;



Constructor Tcell.Create();
begin
  exist := False;
  Resource := -1;
end;

constructor TField.Create(Width, Height: integer);
var
  i, j: integer;
  cell: Tcell;
begin
  // ���������� ������ � ������ ����.
  FWidth := Width;
  FHeight := Height;

  // ������ ������ ������� FCells.
  SetLength(FArea, FWidth);
  // ������� ������� ������.
  for i := 0 to FWidth - 1 do
  begin
    SetLength(FArea[i], FHeight); // ��� ������������, ������ ���������� ������
    for j := 0 to FHeight - 1 do
    begin
      cell := Tcell.Create;
      FArea[i, j] := cell;
    end;
  end;
end;

procedure TForm2.fillStiringGrid(var field: TField; var sg: TStringGrid;
  rowCount, colCount, maxValue: integer);
var
  i, j: integer;
begin
  sg.rowCount := rowCount;
  sg.colCount := colCount;
  rowCount := 10;
  // ������� ������� ������.
  Randomize;
  for i := 0 to StringGrid1.rowCount - 1 do
  begin
    for j := 0 to StringGrid1.colCount - 1 do
    begin
      sg.Cells[i, j] := inttostr(field.FArea[i, j].Resource);
    end;
  end;

end;

procedure TField.fillField(rowCount, colCount, maxValue: integer);
var
  i, j: integer;
begin
  // ������� ������� ������.
  Randomize;
  for i := 0 to rowCount - 1 do
  begin
    for j := 0 to colCount - 1 do
    begin
      FArea[i, j].Resource := Random(maxValue);
    end;
  end;

end;

function TField.findMaxResource(startX, startY: integer ): integer;
var
  i, j, res: integer;
begin
  begin
    i := 0;
    j := 0;
    res := FArea[startX, startY].Resource;
    for i := startX - 1 to startX + 1 do
      for j := startY - 1 to startY + 1 do
      begin
        if IsInside(i, j) then begin
          if (res < (FArea[i, j].Resource)) and ((i <> startX) and (j <> startY))
          then
            res := FArea[i, j].Resource;
        end;
      end;
    maxRes := res;
  end
end;

procedure TField.makeCellExist(X, Y: integer; Resource: integer);
var i,j,res:integer;
begin
   begin
    i := 0;
    j := 0;

    for i := X - 1 to X + 1 do
      for j := Y - 1 to Y + 1 do
      begin
        if isInside(i, j) then begin
          if ((FArea[i, j].Resource)=Resource) and ((i <> X) and (j <> Y))
          then
            FArea[i, j].exist:=true;
        end;
      end;
    maxRes := res;
  end
end;

procedure TField.GrowAllCells;
var
  X, Y: integer;
begin
  for X := 0 to FWidth - 1 do
  begin
    for Y := 0 to FHeight - 1 do
    begin
      if FArea[X, Y].exist then
      begin // ���������, ��� � ������ ���� ����.
        MaxRes := findMaxResource(X, Y );
        makeCellExist(X,Y, maxRes);
      end;
    end;
  end;

  //maxRes == findMaxResource(FWidth, FHeight)
end;

procedure TField.DoStep(Fstep: integer);
var
  i, j: integer;
  res: integer;
begin

  if Fstep = 0 then
  begin
    i := 0;
    j := 0;
    FArea[4, 4].exist:=true;
    res := FArea[4, 4].Resource;
    for i := 3 to 5 do
      for j := 3 to 5 do
      begin
        if (res < (FArea[i, j].Resource)) and ((i <> 4) and (j <> 4)) then
          res := FArea[i, j].Resource;
      end;
    maxRes := res;
  end
  else
  begin
    GrowAllCells();
  end;
end;

procedure TForm2.Button1Click
  (Sender: TObject { ACol, ARow: Integer;   Rect: TRect; State: TGridDrawState } );
var
  i, j: integer;
  // step:Integer;
  field: TField;
begin
  i := 10;
  j := 10;

  field := TField.Create(i, j);
  field.fillField(i, j, 10);
  fillStiringGrid(field, StringGrid1, i, j, 10);

  // ����������� ������

  // ������� ������������ ��������

  field.Fstep := 0;
  field.DoStep(field.Fstep);
  inc(field.Fstep);

end;

procedure TForm2.StringGrid1DrawCell(Field:Tfield; Sender: TObject; ACol, ARow: integer;
  Rect: TRect; State: TGridDrawState);
var
  text: string;
begin
  Field:= Tfield.Farea ;
  StringGrid1.Canvas.Brush.Color := clRed;
  if ((ACol = 4) and (ARow = 4) and (StringGrid1.Cells[ACol, ARow] <> '')) then
    StringGrid1.Canvas.FillRect(Rect);
  if StringGrid1.Cells[ACol, ARow] = '' then
    exit;

  if (abs(ACol - 4) <= 1) and (abs(ARow - 4) <= 1) and
    ((ACol <> 4) or (ARow <> 4)) and
    (FArea[ACol, ARow].exist = true) then
  begin
    StringGrid1.Canvas.Brush.Color := clGreen;
    StringGrid1.Canvas.Font.Color := clWhite;
    StringGrid1.Canvas.FillRect(Rect);
    text := StringGrid1.Cells[ACol, ARow];
    DrawText(StringGrid1.Canvas.Handle, PChar(text), Length(text), Rect,
      DT_CENTER or DT_VCENTER or DT_SINGLELINE);
  end;

end;

procedure TForm2.StringGrid1SelectCell(Sender: TObject; ACol, ARow: integer;
  var CanSelect: boolean);
var

  Resources: String;
begin
  Resources := StringGrid1.Cells[ACol, ARow];
  Label1.Caption := '����� ��������: ' + Resources;

end;

end.
